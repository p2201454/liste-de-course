<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230422114441 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E667840885C');
        $this->addSql('DROP INDEX IDX_23A0E667840885C ON article');
        $this->addSql('ALTER TABLE article ADD prix_unitaire DOUBLE PRECISION NOT NULL, DROP liste_affiliee_id');
        $this->addSql('ALTER TABLE liste_course DROP prix_total');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article ADD liste_affiliee_id INT DEFAULT NULL, DROP prix_unitaire');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E667840885C FOREIGN KEY (liste_affiliee_id) REFERENCES liste_course (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_23A0E667840885C ON article (liste_affiliee_id)');
        $this->addSql('ALTER TABLE liste_course ADD prix_total DOUBLE PRECISION NOT NULL');
    }
}
