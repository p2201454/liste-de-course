<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Article>
 *
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function save(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Article[] Returns an array of Article objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Article
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }


 /**
     * findAllIngredientForRecipe
     *
     * @param  mixed $id
     * @return mixed
     */
    public function findPlusCherByListe($id): mixed
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT id FROM article a WHERE (a.prix_unitaire*a.quantite) = (SELECT MAX(a1.prix_unitaire*a1.quantite) FROM article a1 WHERE a1.id_liste_course_id = ?) AND a.id_liste_course_id = ?

            ';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery([$id, $id]);
        $article = $this->find($resultSet->fetchOne());
        // returns an array of arrays (i.e. a raw data set)
        return $article;
    }

    /**
     * findAllIngredientForRecipe
     *
     * @param  mixed $id
     * @return mixed
     */
    public function findPlusCher(): mixed
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT id FROM article a WHERE (a.prix_unitaire*a.quantite) = (SELECT MAX(a1.prix_unitaire*a1.quantite) FROM article a1)

            ';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery();
        $article = $this->find($resultSet->fetchOne());
        // returns an array of arrays (i.e. a raw data set)
        return $article;
    }

    /**
     * findAllIngredientForRecipe
     *
     * @param  mixed $id
     * @return mixed
     */
    public function findMoinCherByListe($id): mixed
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT id FROM article a WHERE (a.prix_unitaire*a.quantite) = (SELECT MIN(a1.prix_unitaire*a1.quantite) FROM article a1 WHERE a1.id_liste_course_id = ?) AND a.id_liste_course_id = ?

            ';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery([$id,$id]);
        $article = $this->find($resultSet->fetchOne());
        //dd($resultSet->fetchOne());
        return $article;
    }

    /**
     * findAllIngredientForRecipe
     *
     * @param  mixed $id
     * @return mixed
     */
    public function findMoinCher(): mixed
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT id FROM article a WHERE (a.prix_unitaire*a.quantite) = (SELECT MIN(a1.prix_unitaire*a1.quantite) FROM article a1)

            ';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery();
        $article = $this->find($resultSet->fetchOne());
        // returns an array of arrays (i.e. a raw data set)
        return $article;
    }

    public function getSommeTotalByCategAndListe($id, $idCateg): mixed
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT SUM(a.quantite*a.prix_unitaire) prix_total FROM article a WHERE a.categorie_id = ? AND a.id_liste_course_id = ?
            ';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery([$idCateg,$id]);
        
        return $resultSet->fetchOne();
    }

    public function getSommeTotalByCateg($idCateg): mixed
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT SUM(a.quantite*a.prix_unitaire) prix_total FROM article a WHERE a.categorie_id = ?
            ';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery([$idCateg]);
        
        return $resultSet->fetchOne();
    }

    public function getNbArticleByCateg($id, $idListe): mixed
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT COUNT(a.id) prix_total FROM article a WHERE a.categorie_id = ? AND a.id_liste_course_id  = ? ';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery([$id, $idListe]);
        
        return $resultSet->fetchOne();
    }

    public function getNbArticle($id): mixed
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT COUNT(a.id) prix_total FROM article a WHERE a.categorie_id = ?';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery([$id]);
        
        return $resultSet->fetchOne();
    }
}
