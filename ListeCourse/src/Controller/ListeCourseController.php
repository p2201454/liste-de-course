<?php

namespace App\Controller;

use App\Entity\ListeCourse;
use App\Form\ListeCourseType;
use App\Repository\ListeCourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategorieRepository;
use App\Form\CategorieType;
use App\Entity\Categorie;
use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;

#[Route('/')]
class ListeCourseController extends AbstractController
{
    #[Route('/', name: 'app_liste_course_index', methods: ['GET'])]
    public function index(Request $request, ArticleRepository $articleRepository, ListeCourseRepository $listeCourseRepository, CategorieRepository $CategorieRepository): Response
    {
        $listeCourse = new ListeCourse();
        $form = $this->createForm(ListeCourseType::class, $listeCourse, [
            'action' => $this->generateUrl('app_liste_course_new'),
        ]);
        $form->handleRequest($request);

        $categorie = new Categorie();
        $formCategorie = $this->createForm(CategorieType::class, $categorie, [
            'action' => $this->generateUrl('app_categorie_new'),
        ]);
        $formCategorie->handleRequest($request);
      
        $articles = $articleRepository->findAll();
        $categories = $CategorieRepository->findAll();
        $stat_frequence = []; 

        foreach ($categories as $value) {
           $stat_frequence[$value->getId()]['nom'] = $value->getNom();
           $stat_frequence[$value->getId()]['prix_total'] = $articleRepository->getSommeTotalByCateg($value->getId()) ?? 0;
           $stat_frequence[$value->getId()]['percent'] = count($articles) ?  ($articleRepository->getNbArticle($value->getId()) * 100) / count($articles) : 'N/A';
        }

        $prix_total = 0;
        foreach ($articles as $value) {
            $prix_total += $value->getQuantite()*$value->getPrixUnitaire();
        }
        $article_moin_cher = $articleRepository->findMoinCher();
        $article_plus_cher = $articleRepository->findPlusCher();
        return $this->renderForm('liste_course/index.html.twig', [
            'liste_courses' => $listeCourseRepository->findAll(),
            'form' => $form,  
            'categories' => $CategorieRepository->findAll(),
            'formCategorie' => $formCategorie,
            'article_moin_cher'=> $article_moin_cher,
            'article_plus_cher'=> $article_plus_cher,
            'prix_total'=> $prix_total,
            'list_categ'=> $categories,
            'stat_frequence'=> $stat_frequence,
            ]);
    }

    #[Route('/new', name: 'app_liste_course_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ListeCourseRepository $listeCourseRepository): Response
    {
        $listeCourse = new ListeCourse();
        $form = $this->createForm(ListeCourseType::class, $listeCourse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listeCourseRepository->save($listeCourse, true);

            return $this->redirectToRoute('app_liste_course_index', [], Response::HTTP_SEE_OTHER);
        }
        
        return $this->renderForm('liste_course/new.html.twig', [
            'liste_course' => $listeCourse,
            'form' => $form,
            'articles'=> $articles,
            'article_moin_cher'=> $article_moin_cher,
            'article_plus_cher'=> $article_plus_cher,
            'prix_total'=> $prix_total,
            'list_categ'=> $categories,
            'stat_frequence'=> $stat_frequence,
        ]);
    }

    #[Route('/{id}', name: 'app_liste_course_show', methods: ['GET'])]
    public function show(ListeCourse $listeCourse): Response
    {
        return $this->render('liste_course/show.html.twig', [
            'liste_course' => $listeCourse,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_liste_course_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ListeCourse $listeCourse, ListeCourseRepository $listeCourseRepository,  ArticleRepository $articleRepository, CategorieRepository $categRepo): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $article->setIdListeCourse($listeCourse);
            $articleRepository->save($article, true);
            
            return $this->redirectToRoute('app_liste_course_edit', ["id"=> $listeCourse->getId()], Response::HTTP_SEE_OTHER);
        }
        $articles = $listeCourse->getArticles();
        $categories = $categRepo->findAll();

        $stat_frequence = [];

        foreach ($categories as $value) {
           $stat_frequence[$value->getId()]['nom'] = $value->getNom();
           $stat_frequence[$value->getId()]['prix_total'] = $articleRepository->getSommeTotalByCategAndListe($listeCourse->getId(), $value->getId()) ?? 0;
           $stat_frequence[$value->getId()]['percent'] = count($articles) ?  ($articleRepository->getNbArticleByCateg($value->getId(), $listeCourse->getId()) * 100) / count($articles) : 'N/A';
        }

        $prix_total = 0;
        foreach ($articles as $value) {
            $prix_total += $value->getQuantite()*$value->getPrixUnitaire();
        }

        $article_moin_cher = $articleRepository->findMoinCherByListe($listeCourse->getId());
        $article_plus_cher = $articleRepository->findPlusCherByListe($listeCourse->getId());
        dump($article_plus_cher);
        dump($article_moin_cher);
        dump($prix_total);
        dump($stat_frequence);
        // die();
        return $this->renderForm('liste_course/edit.html.twig', [
            'liste_course' => $listeCourse,
            'form' => $form,
            'articles'=> $articles,
            'article_moin_cher'=> $article_moin_cher,
            'article_plus_cher'=> $article_plus_cher,
            'prix_total'=> $prix_total,
            'list_categ'=> $categories,
            'stat_frequence'=> $stat_frequence,
        ]);
    }

    #[Route('/{id}/delete', name: 'app_liste_course_delete', methods: ['POST', 'DLETE'])]
    public function delete(Request $request, ListeCourse $listeCourse, ListeCourseRepository $listeCourseRepository): Response
    {
        // dd($listeCourse);
        if ($this->isCsrfTokenValid('delete'.$listeCourse->getId(), $request->request->get('csrf_token'))) {
            $listeCourseRepository->remove($listeCourse, true);
            // dd('remove');
        }

        return $this->redirectToRoute('app_liste_course_index', [], Response::HTTP_SEE_OTHER);
    }
    #[Route('article/{id}/delete', name: 'app_liste_course_article_delete', methods: ['POST', 'DLETE'])]
    public function deleteArticle(Request $request, Article $article, ArticleRepository $articleRepository): Response
    {
        $id_liste = $article->getIdListeCourse()->getId();
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('csrf_token'))) {
            $articleRepository->remove($article, true);
        }

        return $this->redirectToRoute('app_liste_course_edit', ['id'=>$id_liste], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/acheter', name: 'app_liste_course_toggle_article', methods: ['GET'])]
    public function acheter(Request $request, Article $article, ArticleRepository $articleRepo): Response
    {
        $new_value = !$article->isEstAcheter();
        $article->setEstAcheter($new_value);
        $articleRepo->save($article, true);

        return $this->redirectToRoute('app_liste_course_edit', ['id'=>$article->getIdListeCourse()->getId()], Response::HTTP_SEE_OTHER);
    }

    
}

