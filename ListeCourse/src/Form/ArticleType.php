<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Form\CategorieDataTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ArticleType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('quantite')
            ->add('prixUnitaire')
            ->add('categorie', EntityType::class, [
                'label' => 'Categorie',
                'required' => false,
                'class' => Categorie::class,
                'choice_label'=> 'nom',
                'expanded' => false,
                'multiple'=>false
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
